<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Bpanel4\Shipping\Types\NullShippingOption\NullShippingOption;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Exception;

final class ShippingCostsCalculator
{
    public function __construct(
        private readonly ShippingMethodPriceCalculatorFactory $priceCalculatorFactory,
    ) {
    }

    /**
     * @return array<ShippingOption>
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws Exception
     */
    public function getAvailableOptions(Cart $cart): array
    {
        $output = [];
        $shippingZone = $this->getCartShippingZone($cart);

        if (!$shippingZone instanceof ShippingZone) {
            return [new NullShippingOption()];
        }

        foreach ($shippingZone->getShippingMethods() as $shippingMethod) {
            if (!$shippingMethod->isActive()) {
                continue;
            }

            $output = $this->addShippingOption($shippingMethod, $cart, $output);
        }

        $output = $this->filterOptionsByShippingClass($output, $cart);

        usort(
            $output,
            static fn (ShippingOption $a, ShippingOption $b): int =>
            $a->shippingCostsCalculator->getShippingCosts() <=> $b->shippingCostsCalculator->getShippingCosts()
        );
        return $output;
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     */
    public function calculateShippingCosts(Cart $cart, ShippingMethod $shippingMethod): ?Price
    {
        foreach ($this->getAvailableOptions($cart) as $shippingOption) {
            if ($shippingOption->shippingMethod->getId() === $shippingMethod->getId()) {
                return $shippingOption->shippingCostsCalculator->getShippingCosts();
            }
        }

        return null;
    }

    private function getShippingZonesForState(State $state): ?ShippingZone
    {
        return ShippingZone::whereRelation('states', 'id', '=', $state->getId())
            ->orderBy('order_column')->first();
    }

    /**
     * @param array<array{text: string, shipping_cost: int|null}> $output
     * @return array<ShippingOption>
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    private function addShippingOption(ShippingMethod $shippingMethod, Cart $cart, array $output): array
    {
        $priceCalculator = $this->priceCalculatorFactory->make($shippingMethod, $cart);
        if ($priceCalculator->isEnabled()) {
            $output[] = new ShippingOption($shippingMethod, $priceCalculator);
        }
        return $output;
    }

    private function getCartShippingZone(Cart $cart): null|ShippingZone
    {
        $shippingAddress = $cart->getShippingAddress();
        $state = $shippingAddress->getState();
        return $this->getShippingZonesForState($state);
    }

    private function filterOptionsByShippingClass(array $shippingOptions, Cart $cart)
    {
        $commonShippingClasses = [];
        foreach ($cart->getProducts() as $product) {
            $shippingClasses = $product->getShippingClasses()->pluck('id')->toArray();
            $commonShippingClasses = $commonShippingClasses === [] ?
                $shippingClasses :
                array_intersect($shippingClasses, $commonShippingClasses);
        }

        return array_filter($shippingOptions, function (ShippingOption $option) use ($commonShippingClasses) {
            $shippingOptionClasses = $option->shippingMethod->shippingClasses()->get()->pluck('id')->toArray();
            return count(array_intersect($shippingOptionClasses, $commonShippingClasses)) > 0 ||
                count($shippingOptionClasses) === 0;
        });
    }
}
