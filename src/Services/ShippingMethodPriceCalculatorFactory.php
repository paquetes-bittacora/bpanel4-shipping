<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;

final class ShippingMethodPriceCalculatorFactory
{
    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    public function make(ShippingMethod $shippingMethod, Cart $cart): ShippingMethodPriceCalculator
    {
        $modelClass = $shippingMethod::class;

        /** @var class-string<ShippingMethodPriceCalculator> $priceCalculatorClass */
        $priceCalculatorClass = str_replace(
            ['Models\\', 'ShippingMethods\\'],
            ['', 'Services\\PriceCalculators\\'],
            $modelClass
        ) . 'PriceCalculator';

        $priceCalculator = new $priceCalculatorClass($shippingMethod, $cart);

        $this->checkIfPriceCalculatorIsValid($priceCalculator);

        return $priceCalculator;
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     */
    private function checkIfPriceCalculatorIsValid(object $priceCalculator): void
    {
        if (!$priceCalculator instanceof ShippingMethodPriceCalculator) {
            throw new InvalidShippingMethodPriceCalculatorException();
        }
    }
}
