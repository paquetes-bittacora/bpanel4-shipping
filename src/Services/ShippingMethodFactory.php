<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Illuminate\Database\Eloquent\Model;

final class ShippingMethodFactory
{
    private const SHIPPING_METHODS_NAMESPACE = 'Bittacora\Bpanel4\Shipping\Models\ShippingMethods\\';

    /**
     * @param class-string<Model> $modelName
     */
    public function make(string $modelName, int $modelId): ShippingMethod
    {
        $modelName = self::SHIPPING_METHODS_NAMESPACE . $modelName;
        return $modelName::where('id', '=', $modelId)->firstOrFail();
    }
}
