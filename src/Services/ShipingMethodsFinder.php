<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Exception;
use HaydenPierce\ClassFinder\ClassFinder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

final class ShipingMethodsFinder
{
    private const SHIPPING_METHODS_NAMESPACE = 'Bittacora\Bpanel4\Shipping\Models\ShippingMethods';

    /**
     * Obtiene los modelos definidos en el namespace de métodos de envío
     *
     * @return array<class-string<ShippingMethod&Model>>
     * @throws Exception
     */
    public function getShippingMethodModels(): array
    {
        $output = [];

        $classes = Cache::rememberForever('shippingMethodClasses', function () {
            return ClassFinder::getClassesInNamespace(self::SHIPPING_METHODS_NAMESPACE);
        });

        foreach ($classes as $className) {
            if (in_array(ShippingMethod::class, class_implements($className), true)) {
                $output[] = $className;
            }
        }
        return $output;
    }
}
