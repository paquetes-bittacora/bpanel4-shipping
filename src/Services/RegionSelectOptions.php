<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services;

use Bittacora\LivewireCountryStateSelector\Models\Country;

final class RegionSelectOptions
{
    /**
     * @return array<string, int>
     */
    public function get(): array
    {
        $output = [];
        $countries = Country::orderBy('name', 'ASC')->get();

        foreach ($countries as $country) {
            foreach ($country->states()->orderBy('name', 'ASC')->get() as $state) {
                $output[$country->getName() . ' > ' . $state->getName()] = $state->getId();
            }
        }

        return $output;
    }
}
