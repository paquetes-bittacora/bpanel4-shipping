<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Exceptions;

use Exception;

final class InvalidShippingMethodPriceCalculatorException extends Exception
{
    /** @var string $message */
    protected $message = 'La clase para calcular los gastos de envío no es válida.';
}
