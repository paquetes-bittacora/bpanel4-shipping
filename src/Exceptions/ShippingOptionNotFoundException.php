<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Exceptions;

use Exception;

final class ShippingOptionNotFoundException extends Exception
{
}
