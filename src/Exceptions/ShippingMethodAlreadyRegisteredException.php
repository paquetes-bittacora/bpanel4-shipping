<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Exceptions;

use Exception;

final class ShippingMethodAlreadyRegisteredException extends Exception
{
    /** @var string $message */
    protected $message = 'El método de envío que intentas registrar ya existe';
}
