<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingMethodAlreadyRegisteredException;
use Illuminate\Contracts\Config\Repository;

final class Shipping
{
    public function __construct(private readonly Repository $config)
    {
    }

    /**
     * @param string $name Nombre que se mostrará en los select, etc
     * @param string $className Nombre completo del modelo
     * @throws ShippingMethodAlreadyRegisteredException
     */
    public function registerShippingMethod(string $name, string $className): void
    {
        $currentShippingMethods = $this->getShippingMethods();
        $classBasename = class_basename($className);

        $this->checkIfMethodNameIsAlreadyRegistered($currentShippingMethods, $classBasename);

        $this->config->set('shipping-methods', $currentShippingMethods + [
                $classBasename => [
                    'name' => $name,
                    'class_name' => $className,
                ],
            ]);
    }

    /**
     * @return array<string, array<string|class-string<ShippingMethod>>>
     */
    public function getShippingMethods(): array
    {
        return $this->config->get('shipping-methods') ?? [];
    }

    /**
     * @param array<string, array<string, string>> $currentShippingMethods
     * @throws ShippingMethodAlreadyRegisteredException
     */
    private function checkIfMethodNameIsAlreadyRegistered(array $currentShippingMethods, string $classBasename): void
    {
        if (isset($currentShippingMethods[$classBasename])) {
            throw new ShippingMethodAlreadyRegisteredException();
        }
    }
}
