<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Shipping\Actions\CreateShippingZone;
use Bittacora\Bpanel4\Shipping\Actions\UpdateShippingZone;
use Bittacora\Bpanel4\Shipping\Dtos\CreateShippingZoneDto;
use Bittacora\Bpanel4\Shipping\Dtos\UpdateShippingZoneDto;
use Bittacora\Bpanel4\Shipping\Http\Requests\AddShippingMethodRequest;
use Bittacora\Bpanel4\Shipping\Http\Requests\CreateShippingZoneRequest;
use Bittacora\Bpanel4\Shipping\Http\Requests\UpdateShippingZoneRequest;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Bpanel4\Shipping\Services\RegionSelectOptions;
use Bittacora\Support\Dtos\Exceptions\InvalidDtoClassException;
use Bittacora\Support\Dtos\RequestDtoBuilder;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Str;

final class ShippingZonesAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.index');
        return $this->view->make('bpanel4-shipping::bpanel.zones.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create(RegionSelectOptions $regionSelectOptions): View
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.create');
        return $this->view->make('bpanel4-shipping::bpanel.zones.create', [
            'action' => '',
            'zone' => null,
            'allRegions' => $regionSelectOptions->get(),
        ]);
    }

    /**
     * @throws ValidationException
     * @throws AuthorizationException
     */
    public function store(CreateShippingZoneRequest $request, CreateShippingZone $createShippingZone): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.store');
        /** @var string[] $requestData */
        $requestData = $request->validated();
        $requestData['active'] = is_array($requestData) && isset($requestData['active']) && $requestData['active'];
        $dto = new CreateShippingZoneDto(...$requestData);

        $createShippingZone->handle($dto);

        return $this->redirector->route('bpanel4-shipping.bpanel.zones')->with([
            'alert-success' => __('bpanel4-shipping::zones.created'),
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(ShippingZone $zone, RegionSelectOptions $regionSelectOptions): View
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.edit');
        return $this->view->make('bpanel4-shipping::bpanel.zones.edit', [
            'action' => '',
            'zone' => $zone,
            'allRegions' => $regionSelectOptions->get(),
        ]);
    }

    /**
     * @throws ValidationException|AuthorizationException
     */
    public function update(UpdateShippingZoneRequest $request, UpdateShippingZone $updateShippingZone): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.update');

        /** @var array<string,int,bool> $requestData */
        $requestData = $request->validated();
        $requestData['active'] = is_array($requestData) && isset($requestData['active']) && $requestData['active'];
        $requestData['id'] = (int)$requestData['id'];
        $dto = new UpdateShippingZoneDto(...$requestData);

        $updateShippingZone->handle($dto);

        return $this->redirector->route('bpanel4-shipping.bpanel.zones')->with([
            'alert-success' => __('bpanel4-shipping::zones.updated'),
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function delete(ShippingZone $zone): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.destroy');

        $zone->states()->detach();
        $zone->delete();

        return $this->redirector->route('bpanel4-shipping.bpanel.zones')->with([
            'alert-success' => __('bpanel4-shipping::zones.deleted'),
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function addShippingMethod(AddShippingMethodRequest $request, ShippingZone $zone): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.bpanel.zones.add-shipping-method');
        $shippingMethod = Str::kebab($request->validated()['shipping_method']);

        return $this->redirector->route('bpanel4-shipping.' . $shippingMethod . '.bpanel.create', [
            'zone' => $zone,
        ]);
    }
}
