<?php

namespace Bittacora\Bpanel4\Shipping\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Shipping\Http\Requests\AddShippingClassRequest;
use Bittacora\Bpanel4\Shipping\Http\Requests\UpdateShippingClassRequest;
use Bittacora\Bpanel4\Shipping\Models\ShippingClass;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class ShippingClassesAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
        private readonly UrlGenerator $urlGenerator,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function index(): View
    {
        $this->authorize('bpanel4-shipping.bpanel.classes.index');
        return $this->view->make('bpanel4-shipping::bpanel.classes.index');
    }

    public function create(): View
    {
        $this->authorize('bpanel4-shipping.bpanel.classes.create');
        return $this->view->make('bpanel4-shipping::bpanel.classes.create', [
            'action' => $this->urlGenerator->route('bpanel4-shipping.bpanel.classes.store'),
            'class' => null
        ]);
    }

    public function edit(ShippingClass $class): View
    {
        $this->authorize('bpanel4-shipping.bpanel.classes.edit');
        return $this->view->make('bpanel4-shipping::bpanel.classes.edit', [
            'action' => $this->urlGenerator->route('bpanel4-shipping.bpanel.classes.update', ['class' => $class]),
            'class' => $class
        ]);
    }

    public function update(UpdateShippingClassRequest $request, ShippingClass $class): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.bpanel.classes.update');

        try {
            $class->name = $request->validated('name');
            $class->active = $request->validated('active') ?? false;
            $class->save();
            return $this->redirector->route('bpanel4-shipping.bpanel.classes')->with([
                'alert-success' => 'La clase de envío se actualizó correctamente',
            ]);
        } catch (Exception $e) {
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al intentar actualizar la clase de envío',
            ]);
        }
    }

    public function store(AddShippingClassRequest $request): RedirectResponse
    {
        try {
            $shippingClass = new ShippingClass();
            $shippingClass->name = $request->validated('name');
            $shippingClass->active = $request->validated('active') ?? false;
            $shippingClass->save();
            return $this->redirector->route('bpanel4-shipping.bpanel.classes')->with([
                'alert-success' => 'La clase de envío se creó correctamente',
            ]);
        } catch (Exception $e) {
            report($e);
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al intentar crear la clase de envío',
            ]);
        }
    }

    public function destroy(ShippingClass $class): RedirectResponse
    {
        $class->delete();
        return $this->redirector->back()->with([
            'alert_success' => 'La clase de envío se eliminó correctamente'
        ]);
    }
}