<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class AddShippingClassRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|unique:shipping_zones,name',
            'active' => 'nullable'
        ];
    }
}
