<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class UpdateShippingClassRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'id' => 'required',
            'name' =>[
                'required',
                'string',
                Rule::unique('shipping_zones')->ignore($this->id),
            ],
            'active' => 'nullable'
        ];
    }
}
