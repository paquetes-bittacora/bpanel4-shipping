<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class AddShippingMethodRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(): array
    {
        return [
            'shipping_method' => 'required|string',
        ];
    }
}
