<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Requests;

use Bittacora\Bpanel4\Shipping\Validation\ShippingZoneValidator;
use Illuminate\Foundation\Http\FormRequest;

final class CreateShippingZoneRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(ShippingZoneValidator $shippingZoneValidator): array
    {
        return $shippingZoneValidator->getZoneCreationValidationFields();
    }
}
