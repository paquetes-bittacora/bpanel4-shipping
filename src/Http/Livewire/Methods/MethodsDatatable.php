<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Livewire\Methods;

use Bittacora\Bpanel4\Shipping\Services\ShipingMethodsFinder;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class MethodsDatatable extends DataTableComponent
{
    public ?int $shippingZoneId = null;

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name'),
            Column::make('Activo', 'active')
                ->format(
                    fn(
                        $value,
                        $row,
                        Column $column
                    ) => view('bpanel4-shipping::bpanel.methods.livewire.datatable-fields.active')
                        ->with(['row' => $row])
                ),
            Column::make('Acciones', 'id')->view('bpanel4-shipping::bpanel.methods.livewire.datatable-fields.actions'),
        ];
    }

    /**
     * @return Builder
     * @throws Exception
     */
    public function query(): Builder
    {
        // No puedo usar inyección de dependencias en esta clase
        $modelNames = (new ShipingMethodsFinder())->getShippingMethodModels();

        $builder = null;
        foreach ($modelNames as $modelName) {
            $builder = $this->getBpModelBuilder($modelName, $builder);
        }
        return $builder/*->orderBy('name', 'ASC')*/ ;
    }

    public function rowView(): string
    {
        return 'bpanel4-shipping::bpanel.methods.livewire.methods-datatable';
    }

    private function getBpModelBuilder(string $modelName, ?Builder $builder): Builder
    {
        /** @var Model $model */
        $model = new $modelName();
        $modelColumn = DB::raw('"' . addslashes($modelName) . '" as model');
        if (null === $builder) {
            $query = $model->select([
                $model->table . '.name as name',
                $model->table . '.active as active',
                $model->table . '.id as id',
                $modelColumn,
            ])
                ->where('shipping_zone_id', '=', $this->shippingZoneId ?? 0)->withoutGlobalScopes();
            return $query;
        }
        $query = $builder->union($model->select([
            $model->table . '.name as name',
            $model->table . '.active as active',
            $model->table . '.id as id',
            $modelColumn,
        ])->where('shipping_zone_id', '=', $this->shippingZoneId ?? 0))->withoutGlobalScopes();
        return $query;
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
