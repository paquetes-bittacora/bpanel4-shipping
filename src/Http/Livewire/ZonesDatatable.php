<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Http\Livewire;

use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class ZonesDatatable extends DataTableComponent
{
    public bool $reordering = true;

    public string $reorderingMethod = 'reorder';
    public ?string $defaultSortColumn = 'order_column';
    public string $defaultSortDirection = 'ASC';

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make('Nombre', 'name')->sortable(),
            Column::make('Activo', 'active')->view('bpanel4-shipping::bpanel.livewire.datatable-fields.active')->sortable(),
            Column::make('Acciones', 'id')->view('bpanel4-shipping::bpanel.livewire.datatable-fields.actions'),
        ];
    }

    /**
     * @return Builder<ShippingZone>
     */
    public function query(): Builder
    {
        return ShippingZone::query()
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn ($query, $term) => $query
                    ->where('name', 'like', '%' . $term . '%')
            );
    }

    public function rowView(): string
    {
        return 'bpanel4-shipping::bpanel.livewire.zones-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            ShippingZone::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }

    public function reorder($list): void
    {
        foreach ($list as $item) {
            ShippingZone::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
