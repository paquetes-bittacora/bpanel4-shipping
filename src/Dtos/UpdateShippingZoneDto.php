<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Dtos;

use Bittacora\Support\Dtos\Dto;

final class UpdateShippingZoneDto implements Dto
{
    /**
     * @param array<int> $states
     */
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly bool $active,
        public readonly array $states,
    ) {
    }

    public static function map(array $data): self
    {
        return new self(
            (int) $data['id'],
            $data['name'],
            isset($data['active']) && (1 === (int)$data['active'] || true === $data['active']),
            array_map(fn ($x) => (int) $x, $data['states']),
        );
    }
}
