<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Dtos;

use Bittacora\Support\Dtos\Dto;

final class CreateShippingZoneDto implements Dto
{
    /**
     * @param array<int> $states
     */
    public function __construct(
        public readonly string $name,
        public readonly array $states,
        public readonly bool $active = false,
    ) {
    }

    public static function map(array $data): self
    {
        return new self(
            $data['name'],
            array_map(fn ($x) => (int) $x, $data['states']),
            isset($data['active']) && (1 === (int)$data['active'] || true === $data['active']),
        );
    }
}
