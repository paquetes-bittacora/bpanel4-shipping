<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Contracts;

interface ShippingMethod
{
    public function getId(): int;

    public function getShippingZoneId(): int;

    public function isActive(): bool;

    public function getName(): string;
}
