<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Contracts;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Types\Price;

interface ShippingMethodPriceCalculator
{
    public function __construct(ShippingMethod $shippingMethod, Cart $cart);

    /**
     * Devuelve el texto a mostrar en el listado de opciones de envío.
     */
    public function getOptionText(): string;

    /**
     * Devuelve si el método de envío está habilitado o no. Normalmente devolverá true pero algunos métodos de envío
     * pueden necesitar deshabilitarse según condiciones del carrito.
     */
    public function isEnabled(): bool;

    /**
     * Devuelve el importe total del envío para el pedido.
     */
    public function getShippingCosts(): ?Price;
}
