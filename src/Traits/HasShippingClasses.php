<?php

namespace Bittacora\Bpanel4\Shipping\Traits;

use Bittacora\Bpanel4\Shipping\Models\ShippingClass;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasShippingClasses
{
    public function shippingClasses(): MorphToMany
    {
        return $this->morphToMany(ShippingClass::class, 'shipping_method', 'shipping_classes_methods');
    }
}