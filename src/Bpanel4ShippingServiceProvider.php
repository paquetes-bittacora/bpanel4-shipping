<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping;

use Bittacora\Bpanel4\Shipping\Commands\InstallCommand;
use Bittacora\Bpanel4\Shipping\Commands\InstallShippinClassesCommand;
use Bittacora\Bpanel4\Shipping\Http\Livewire\ClassesDatatable;
use Bittacora\Bpanel4\Shipping\Http\Livewire\Methods\MethodsDatatable;
use Bittacora\Bpanel4\Shipping\Http\Livewire\ZonesDatatable;
use Bittacora\Bpanel4\Shipping\View\ShippingClassSelectComponent;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Livewire\LivewireManager;

final class Bpanel4ShippingServiceProvider extends ServiceProvider
{
    private const MODULE_PREFIX = 'bpanel4-shipping';

    public function boot(
        LivewireManager $livewire,
        BladeCompiler $blade,
    ): void {
        $this->commands([InstallCommand::class, InstallShippinClassesCommand::class]);

        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::MODULE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::MODULE_PREFIX);

        $livewire->component(self::MODULE_PREFIX . '::livewire.zones-table', ZonesDatatable::class);
        $livewire->component(self::MODULE_PREFIX . '::livewire.classes-table', ClassesDatatable::class);
        $livewire->component(self::MODULE_PREFIX . '::livewire.methods-table', MethodsDatatable::class);

        $blade->component(ShippingClassSelectComponent::class, self::MODULE_PREFIX . '::shipping-class-select');
    }
}
