<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping;

use Illuminate\Support\Facades\Facade;

final class ShippingFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return Shipping::class;
    }
}
