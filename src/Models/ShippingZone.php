<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Models;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Services\ShipingMethodsFinder;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

/**
 * Bittacora\Bpanel4\Shipping\Models\ShippingZone
 *
 * @property int $id
 * @property string $name
 * @property int $order_column
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ShippingZone newModelQuery()
 * @method static Builder|ShippingZone newQuery()
 * @method static Builder|ShippingZone ordered(string $direction = 'asc')
 * @method static Builder|ShippingZone query()
 * @method static Builder|ShippingZone whereActive($value)
 * @method static Builder|ShippingZone whereCreatedAt($value)
 * @method static Builder|ShippingZone whereId($value)
 * @method static Builder|ShippingZone whereName($value)
 * @method static Builder|ShippingZone whereOrderColumn($value)
 * @method static Builder|ShippingZone whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class ShippingZone extends Model implements Sortable
{
    use SortableTrait;
    use SoftDeletes;

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'active' => 'bool',
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return BelongsToMany<State>
     */
    public function states(): BelongsToMany
    {
        return $this->belongsToMany(State::class);
    }

    /**
     * @param array<int> $states
     */
    public function setStates(array $states): void
    {
        $this->states()->sync($states);
    }

    /**
     * @return EloquentCollection<array-key, State>
     */
    public function getStates(): EloquentCollection
    {
        return $this->states()->get();
    }

    /**
     * @return array<ShippingMethod>
     * @throws Exception
     *
     * No he usado una relación polimórfica de Laravel porque a la hora de hacer el datatable que lista los distintos
     * métodos de envío de una zona de envío, necesitaba saber de antemano qué modelos podían estar relacionados con
     * la zona de envío. Después ya vi que se podía hacer con una relación polimórfica normal, pero por no rehacer el
     * código y los tests, lo dejo como está.
     */
    public function getShippingMethods(): array
    {
        $output = [];
        $shippingMethods = (new ShipingMethodsFinder())->getShippingMethodModels();

        foreach ($shippingMethods as $shippingMethod) {
            $relatedModels = $shippingMethod::where('shipping_zone_id', '=', $this->id)->get()->all();

            foreach($relatedModels as $relatedModel) {
                if ($relatedModel instanceof $shippingMethod) {
                    $output[] = $relatedModel;
                }
            }
        }

        return $output;
    }
}
