<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


final class ShippingClass extends Model
{
    use SoftDeletes;

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'active' => 'bool',
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

}
