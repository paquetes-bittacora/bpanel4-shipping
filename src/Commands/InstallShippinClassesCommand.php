<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;

final class InstallShippinClassesCommand extends Command
{
    /** @var string  */
    protected $signature = 'bpanel4-shipping:install-shipping-classes';

    /** @var string  */
    protected $description = 'Instala la sección de clases de envío de bPanel4 Shipping.';

    private const SUBMODULES = ['classes'];
    
    public function handle(AdminMenu $adminMenu): void
    {
        $this->createShippingClassesTabs();
    }

    private function createShippingClassesTabs(): void
    {
        $this->comment('Añadiendo tabs...');

        Tabs::createItem(
            'bpanel4-shipping.bpanel.classes',
            'bpanel4-shipping.bpanel.classes',
            'bpanel4-shipping.bpanel.classes',
            'Listado',
            'fa fa-list'
        );

        Tabs::createItem(
            'bpanel4-shipping.bpanel.classes',
            'bpanel4-shipping.bpanel.classes.create',
            'bpanel4-shipping.bpanel.classes.create',
            'Añadir clase de envío',
            'fa fa-plus'
        );
    }
}
