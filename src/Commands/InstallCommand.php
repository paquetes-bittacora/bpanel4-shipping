<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Commands;

use Bittacora\AdminMenu\AdminMenu;
use Bittacora\Tabs\Tabs;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string  */
    protected $signature = 'bpanel4-shipping:install';

    /** @var string  */
    protected $description = 'Instala el paquete para administrar los gastos de envío.';

    private const SUBMODULES = ['zones', 'classes'];
    private const PERMISSIONS = ['index', 'create', 'edit', 'delete', 'store', 'update', 'destroy', 'add-shipping-method'];

    public function handle(AdminMenu $adminMenu): void
    {
        $this->createMenuEntries($adminMenu);
        $this->createShippingZonesTabs();
        $this->giveAdminPermissions();
    }

    private function createMenuEntries(AdminMenu $adminMenu): void
    {
        $this->comment('Añadiendo módulo de gastos de envío al menú de administración...');

        $adminMenu->createGroup('ecommerce', 'Tienda', 'far fa-shopping-cart');
        $adminMenu->createModule(
            'ecommerce',
            'bpanel4-shipping.bpanel',
            'Gastos de envío',
            'far fa-shipping-fast'
        );
        $adminMenu->createAction(
            'bpanel4-shipping.bpanel',
            'Zonas de envío',
            'zones',
            'far fa-globe-europe'
        );
        $adminMenu->createAction(
            'bpanel4-shipping.bpanel',
            'Métodos de envío',
            'methods',
            'far fa-ballot-check'
        );
        $adminMenu->createAction(
            'bpanel4-shipping.bpanel',
            'Clases de envío',
            'classes',
            'far fa-cubes'
        );
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::SUBMODULES as $submodule) {
            $submodulePermission = Permission::firstOrCreate(['name' => 'bpanel4-shipping.bpanel.' . $submodule]);
            $adminRole->givePermissionTo($submodulePermission);
            foreach (self::PERMISSIONS as $permissionName) {
                $permission = $submodule . '.' . $permissionName;
                $permission = Permission::firstOrCreate(['name' => 'bpanel4-shipping.bpanel.' . $permission]);
                $adminRole->givePermissionTo($permission);
            }
        }
    }

    private function createShippingZonesTabs(): void
    {
        $this->comment('Añadiendo tabs...');

        Tabs::createItem(
            'bpanel4-shipping.bpanel.zones',
            'bpanel4-shipping.bpanel.zones',
            'bpanel4-shipping.bpanel.zones',
            'Listado',
            'fa fa-list'
        );

        Tabs::createItem(
            'bpanel4-shipping.bpanel.zones',
            'bpanel4-shipping.bpanel.zones.create',
            'bpanel4-shipping.bpanel.zones.create',
            'Añadir zona de envío',
            'fa fa-plus'
        );
    }
}
