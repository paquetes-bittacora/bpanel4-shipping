<?php

namespace Bittacora\Bpanel4\Shipping\View;

use Bittacora\Bpanel4\Shipping\Models\ShippingClass;
use Illuminate\View\Component;


class ShippingClassSelectComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public ?array $selectedValues = [])
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('bpanel4-shipping::bpanel.classes.shipping-class-selector', [
            'shippingClasses' => ShippingClass::where('active', true)->orderBy('name', 'ASC')->pluck('name', 'id'),
            'selectedValues' => $this->selectedValues
        ]);
    }
}
