<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Actions;

use Bittacora\Bpanel4\Shipping\Dtos\UpdateShippingZoneDto;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Bpanel4\Shipping\Validation\ShippingZoneValidator;
use Illuminate\Validation\ValidationException;

final class UpdateShippingZone
{
    public function __construct(private readonly ShippingZoneValidator $shippingZoneValidator)
    {
    }

    /**
     * @throws ValidationException
     */
    public function handle(UpdateShippingZoneDto $dto): void
    {
        $this->shippingZoneValidator->validateUpdate((array) $dto);
        $shippingZone = ShippingZone::where('id', $dto->id)->firstOrFail();
        $shippingZone->setName($dto->name);
        $shippingZone->setActive($dto->active);
        $shippingZone->setStates($dto->states);

        $shippingZone->save();
    }
}
