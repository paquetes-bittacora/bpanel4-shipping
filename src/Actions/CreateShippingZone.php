<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Actions;

use Bittacora\Bpanel4\Shipping\Dtos\CreateShippingZoneDto;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Bpanel4\Shipping\Validation\ShippingZoneValidator;
use Illuminate\Validation\ValidationException;

final class CreateShippingZone
{
    public function __construct(
        private readonly ShippingZoneValidator $shippingZoneValidator,
    ) {
    }

    /**
     * @phpstan-param CreateShippingZoneDto $dto
     * @throws ValidationException
     */
    public function handle(CreateShippingZoneDto $dto): void
    {
        $this->shippingZoneValidator->validateCreation((array) $dto);
        $shippingZone = new ShippingZone();
        $shippingZone->setName($dto->name);
        $shippingZone->setActive($dto->active);
        $shippingZone->save();

        $shippingZone->setStates($dto->states);
    }
}
