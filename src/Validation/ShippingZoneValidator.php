<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Validation;

use Illuminate\Validation\Factory;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

final class ShippingZoneValidator
{
    public function __construct(private readonly Factory $validation)
    {
    }

    /**
     * @return array<string, string>
     */
    public function getZoneCreationValidationFields(): array
    {
        return [
            'name' => 'required|string|unique:shipping_zones,name',
            'states' => 'required',
            'states.*' => 'exists:states,id',
            'active' => 'sometimes|bool',
        ];
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateCreation(array $data): void
    {
        $this->validation->validate($data, $this->getZoneCreationValidationFields());
    }

    /**
     * @return array<string, string>
     */
    public function getZoneUpdateValidationFields(int $id): array
    {
        $rules = $this->getZoneCreationValidationFields() + [
            'id' => 'required|numeric',
        ];

        $rules['name'] = [
            'required',
            'string',
            Rule::unique('shipping_zones', 'name')->ignore($id),
        ];

        return $rules;
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateUpdate(array $data): void
    {
        $this->validation->validate($data, $this->getZoneUpdateValidationFields((int) $data['id']));
    }
}
