<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Types;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;

class ShippingOption
{
    public function __construct(
        public readonly ShippingMethod $shippingMethod,
        public readonly ShippingMethodPriceCalculator $shippingCostsCalculator,
    ) {
    }

    public function getKey(): string
    {
        $key = $this->shippingMethod::class . '-' . $this->shippingMethod->getId();
        return str_replace('\\', '', $key);
    }
}
