<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Types\NullShippingOption;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;

final class NullShippingMethodPriceCalculator implements ShippingMethodPriceCalculator
{
    public function __construct(public readonly ShippingMethod $shippingMethod, public readonly Cart $cart)
    {
    }

    public function getOptionText(): string
    {
        return '-';
    }

    public function isEnabled(): bool
    {
        return true;
    }

    public function getShippingCosts(): ?Price
    {
        return null;
    }
}
