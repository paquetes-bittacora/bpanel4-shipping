<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Types\NullShippingOption;

use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;

final class NullShippingMethod implements ShippingMethod
{
    public function getId(): int
    {
        return 0;
    }

    public function getShippingZoneId(): int
    {
        return 0;
    }

    public function isActive(): bool
    {
        return true;
    }

    public function getName(): string
    {
        return '-';
    }
}
