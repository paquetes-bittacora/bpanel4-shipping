<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Types\NullShippingOption;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;

final class NullShippingOption extends ShippingOption
{
    public readonly ShippingMethod $shippingMethod;
    public readonly ShippingMethodPriceCalculator $shippingCostsCalculator;

    /** @phpstan-ignore-next-line  */
    public function __construct()
    {
        $this->shippingMethod = new NullShippingMethod();
        $this->shippingCostsCalculator = new NullShippingMethodPriceCalculator($this->shippingMethod, new Cart());
    }

    public function getKey(): string
    {
        return 'null-shipping-option';
    }
}
