<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('shipping_zone_state', static function (Blueprint $table) {
            $table->unsignedBigInteger('shipping_zone_id');
            $table->unsignedBigInteger('state_id');

            $table->foreign('shipping_zone_id')->references('id')->on('shipping_zones');
            $table->foreign('state_id')->references('id')->on('states');

            $table->unique(['shipping_zone_id', 'state_id']);
        });
    }

    public function down(): void
    {
        Schema::drop('shipping_zone_state');
    }
};
