<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('shipping_classes_methods', static function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shipping_class_id');
            $table->morphs('shipping_method', 'shipping_method_idx');
            $table->timestamps();

            $table->foreign('shipping_class_id')->references('id')->on('shipping_classes')
                ->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::drop('shipping_classes_shipping_methods');
    }
};
