<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Database\Factories;

use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ShippingZone>
 */
final class ShippingZoneFactory extends Factory
{
    /**
     * @var class-string<ShippingZone>
     */
    protected $model = ShippingZone::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'order_column' => $this->faker->numberBetween(),
            'active' => true,
        ];
    }
}
