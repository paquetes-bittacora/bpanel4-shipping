<?php

declare(strict_types=1);

return [
    'new' => 'Nueva zona de envío',
    'edit' => 'Editar zona de envío',
    'new-shipping-zone' => 'Nueva zona de envío',
    'edit-shipping-zone' => 'Editar zona de envío',
    'name' => 'Nombre',
    'active' => 'Activado',
    'add-shipping-method' => 'Añadir método de envío',
    'cancel' => 'Cancelar',
    'save' => 'Guardar',
    'shipping-methods' => 'Métodos de envío',
    'shipping-regions' => 'Regiones',
    'new-shipping-class' => 'Nueva clase de envío',
    'edit-shipping-class' => 'Editar clase de envío',
];
