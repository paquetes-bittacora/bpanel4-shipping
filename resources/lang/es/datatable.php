<?php

declare(strict_types=1);

return [
    'zones' => 'Zonas de envío',
    'classes' => 'Clases de envío',
];
