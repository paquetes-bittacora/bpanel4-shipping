<?php

declare(strict_types=1);

return [
    'bpanel4-shipping' => 'Gastos de envío',
    'bpanel' => 'Configuración',
    // Las siguientes traducciones habrá que sacarlas a cada plugin de gastos
    // de envío
    'free-shipping' => 'Envío gratis',
    'fixed-amount' => 'Precio fijo',
];
