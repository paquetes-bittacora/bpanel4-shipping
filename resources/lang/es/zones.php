<?php

declare(strict_types=1);

return [
    'created' => 'La zona de envío ha sido creada',
    'updated' => 'La zona de envío ha sido actualizada',
    'deleted' => 'La zona de envío ha sido eliminada',
];
