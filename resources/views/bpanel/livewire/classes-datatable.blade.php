<td>
    {{ $row->getName() }}
</td>

<td>
    @livewire('utils::datatable-default', ['fieldName' => 'active', 'model' => $row, 'value' => $row->active, 'size' => 'xxs'], key('active-class-'.$row->id))
</td>
<td>
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', ['actions' => ["edit", "delete"], 'scope' => 'bpanel4-shipping.bpanel.classes', 'model' => $row, 'permission' => ['edit', 'delete'], 'id' => $row->id, 'message' => 'la clase de envío?'], key('class-buttons-'.$row->id))
    </div>
</td>
