@extends('bpanel4::layouts.bpanel-app')

@section('title', 'Zonas de envío')

@section('content')

    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-shipping::datatable.zones') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('bpanel4-shipping::livewire.zones-table', [], key('bpanel4-shipping-zones-datatable'))
        </div>
    </div>


@endsection

@push('scripts')
    @vite('vendor/bittacora/bpanel4-panel/resources/assets/js/livewire-sortable.js')
@endpush
