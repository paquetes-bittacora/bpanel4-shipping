@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-shipping::form.new'))

@section('content')
    @include('bpanel4-shipping::bpanel.zones._form', ['panelTitle' => __('bpanel4-shipping::form.new-shipping-zone')])
@endsection
