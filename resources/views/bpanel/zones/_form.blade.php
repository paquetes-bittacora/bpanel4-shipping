<form autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 mb-lg-3">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
        </div>
        @csrf
        @livewire('form::input-text', [
        'name' => 'name',
        'labelText' => __('bpanel4-shipping::form.name'),
        'required'=>true,
        'value' => old('name') ?? $zone?->getName()
        ])
        @livewire('form::input-checkbox', [
        'name' => 'active',
        'value' => 1,
        'checked' => $zone?->isActive() ?? false,
        'labelText' => __('bpanel4-shipping::form.active'), 'bpanelForm' => true
        ])
        <div class="form-group form-row">
            <div class="col-sm-3 col-form-label text-sm-right">
                <label for="id-form-field-1" class="mb-0  ">
                    <span class="text-danger">*</span> Regiones
                </label>
            </div>
            <div class="col-sm-7">
                @livewire('form::dual-list-box', ['name' => 'states[]', 'idField' => 'regionsSelect',
                'allValues' => array_flip($allRegions), 'height' => '232px', 'selectedValues' => $zone?->states()->pluck('id')->toArray() ?? []])
                <p class="text-muted small">Para añadir rápidamente todas las regiones de un país, filtre
                                            en la columna de la izquierda por el nombre del país y haga click en el
                                            botón
                    <i class="fa fa-arrow-right"></i><i class="fa fa-arrow-right"></i></p>
            </div>
        </div>
        <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
            @livewire('form::save-button',['theme'=>'update'])
            @livewire('form::save-button',['theme'=>'reset'])
        </div>
        @if (null !== $zone?->getId())
            <input type="hidden" name="id" value="{{ $zone->getId() }}">
        @endif
    </div>
</form>
<div class="card bcard mt-4">
    <div class="card-header bgc-secondary-d1 text-white border-0">
        <h4 class="text-120 mb-0">
            <span class="text-90">{{ __('bpanel4-shipping::form.shipping-methods') }}</span>
        </h4>
    </div>
    <div class="p-3">
        @livewire('bpanel4-shipping::livewire.methods-table', ['shippingZoneId' => $zone?->getId()])
    </div>
    @include('bpanel4-shipping::bpanel.methods._shipping-methods-popup')
</div>
