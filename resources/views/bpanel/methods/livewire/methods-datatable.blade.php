@php
    // @todo sacar esto a otro sitio para no tener que usar @php
    $row = $row->model::where('id', '=', $row->id)->firstOrFail();
@endphp

<td>
    {{ $row->getName() }}
</td>

<td>
    @livewire('utils::datatable-default', [
        'fieldName' => 'active',
        'model' => $row,
        'value' => $row->active,
        'size' => 'xxs'
    ], key('active-method-'.$row->id))
</td>
<td>
    <div class="text-center">
        @livewire('utils::datatable-action-buttons', [
            'actions' => ["edit", "delete"],
            'scope' => 'bpanel4-shipping.' . Str::kebab(class_basename($row)) . '.bpanel',
            'model' => $row,
            'permission' => ['edit', 'delete'],
            'id' => $row->id,
            'message' => 'el método de envío?',
            'routeParams' => [
                'edit' => [
                    'model' => $row->getId(),
                ],
                'destroy' => [
                    'model' => $row->getId(),
                ]
            ]
        ], key('zone-buttons-'.$row->id))
    </div>
</td>
