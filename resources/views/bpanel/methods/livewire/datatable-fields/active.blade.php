@php
            // @todo sacar esto a otro sitio para no tener que usar @php
            $row = $row->model::where('id', '=', $row->id)->firstOrFail();
@endphp
@livewire('utils::datatable-default', [
    'fieldName' => 'active',
    'model' => $row,
    'value' => $row->active,
    'size' => 'xxs'
], key('active-method-'.$row->id))
