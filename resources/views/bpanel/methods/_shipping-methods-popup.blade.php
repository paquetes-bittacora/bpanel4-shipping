@if (null !== $zone?->getId())
    <div class="d-flex justify-content-center">
        <button type="button" class="col-3 btn btn-primary btn-bold px-4 mx-2 my-3" data-toggle="modal"
                data-target="#shippingMethodsModal">
            <i class="fa fa-plus mr-1" aria-hidden="true"></i>
            &nbsp;Añadir método de envío
        </button>
    </div>
    <div class="modal fade" id="shippingMethodsModal" tabindex="-1" aria-labelledby="shippingMethodsModalLabel"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary-d3" id="shippingMethodsModalLabel">
                        {{ __('bpanel4-shipping::form.add-shipping-method') }}
                    </h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <form method="post" action="{{ route('bpanel4-shipping.bpanel.zones.add-shipping-method', [
                        'zone' => $zone->getId(),
                    ]) }}">
                    <div class="modal-body">
                        @csrf
                        <select class="form-control select2" name="shipping_method">
                            @foreach(\Bittacora\Bpanel4\Shipping\ShippingFacade::getShippingMethods() as $key => $method)
                                <option value="{{ $key }}">{{ $method['name'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary px-4" data-dismiss="modal">
                            {{ __('bpanel4-shipping::form.cancel') }}
                        </button>

                        <button type="submit" class="btn btn-primary">
                            {{ __('bpanel4-shipping::form.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@else
    <div class="d-flex justify-content-center mt-4">
        <div class="col-5">
            <x-bpanel4-warning message="Para añadir métodos de envío, primero debe guardar la zona"/>
        </div>
    </div>
@endif
