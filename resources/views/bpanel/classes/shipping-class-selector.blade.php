<div>
    @livewire('form::select', [
        'name' => 'shipping_classes[]',
        'allValues' => $shippingClasses,
        'labelText' => __('bpanel4-shipping::classes.classes'),
        'selectedValues' => $selectedValues,
        'emptyValue' => true,
        'emptyValueText' => 'Cualquier clase de envío',
        'multiple' => true
    ])
</div>

@pushonce('scripts')
    <script>
      document.addEventListener('DOMContentLoaded', function () {
        $('.select2').select2();
      });
    </script>
@endpushonce