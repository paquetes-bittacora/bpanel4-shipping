@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-shipping::form.edit'))

@section('content')
    @include('bpanel4-shipping::bpanel.classes._form', ['panelTitle' => __('bpanel4-shipping::form.edit-shipping-class')])
@endsection
