<form autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 mb-lg-3">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
        </div>
        @csrf
        @livewire('form::input-text', [
        'name' => 'name',
        'labelText' => __('bpanel4-shipping::form.name'),
        'required'=>true,
        'value' => old('name') ?? $class?->getName()
        ])
        @livewire('form::input-checkbox', [
        'name' => 'active',
        'value' => 1,
        'checked' => $class?->isActive() ?? false,
        'labelText' => __('bpanel4-shipping::form.active'), 'bpanelForm' => true
        ])
        <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
            @livewire('form::save-button',['theme'=>'update'])
            @livewire('form::save-button',['theme'=>'reset'])
        </div>
        @if (null !== $class?->getId())
            <input type="hidden" name="id" value="{{ $class->getId() }}">
        @endif
    </div>
</form>