<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Shipping\Http\Controllers\ShippingClassesAdminController;
use Bittacora\Bpanel4\Shipping\Http\Controllers\ShippingZonesAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/envio')->name('bpanel4-shipping.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        // Zonas
        Route::get('/zonas', [ShippingZonesAdminController::class, 'index'])->name('zones');
        Route::get('/zonas/crear', [ShippingZonesAdminController::class, 'create'])->name('zones.create');
        Route::post('/zonas/crear', [ShippingZonesAdminController::class, 'store'])->name('zones.store');
        Route::get('/zonas/{zone}/editar', [ShippingZonesAdminController::class, 'edit'])->name('zones.edit');
        Route::post('/zonas/{zone}/editar', [ShippingZonesAdminController::class, 'update'])->name('zones.update');
        Route::delete('/zonas/{zone}/borrar', [ShippingZonesAdminController::class, 'delete'])->name('zones.destroy');
        Route::post('/zonas/{zone}/anadir-metodo-envio', [ShippingZonesAdminController::class, 'addShippingMethod'])
            ->name('zones.add-shipping-method');

        // Clases de envío
        Route::get('/clases', [ShippingClassesAdminController::class, 'index'])->name('classes');
        Route::get('/clases/crear', [ShippingClassesAdminController::class, 'create'])->name('classes.create');
        Route::post('/clases/crear', [ShippingClassesAdminController::class, 'store'])->name('classes.store');
        Route::get('/clases/{class}/editar', [ShippingClassesAdminController::class, 'edit'])->name('classes.edit');
        Route::post('/clases/{class}/actualizar', [ShippingClassesAdminController::class, 'update'])->name('classes.update');
        Route::delete('/clases/{class}/eliminar', [ShippingClassesAdminController::class, 'destroy'])->name('classes.destroy');
    });
