<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Tests\Feature;

use Bittacora\Bpanel4Users\Database\Factories\UserFactory;
use Bittacora\LivewireCountryStateSelector\Database\Factories\StateFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Creo este test para evitar que se repita un fallo que he visto mientras probaba manualmente.
 */
final class ShippingZonesAdminControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testSePuedeCrearUnaZonaDeEnvio(): void
    {
        $this->withoutExceptionHandling();

        (new StateFactory())->createOne();

        $this->actingAs((new UserFactory())->withPermissions(
            'bpanel4-shipping.bpanel.zones.store',
            'bpanel4-shipping.bpanel.zones.index',
        )->createOne());
        $response = $this->followingRedirects()->post(route('bpanel4-shipping.bpanel.zones.store'), [
            'name' => 'Prueba',
            'states' => [1],
        ]);

        $response->assertOk();
    }
}
