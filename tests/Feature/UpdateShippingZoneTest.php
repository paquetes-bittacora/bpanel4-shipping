<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Tests\Feature;

use Bittacora\Bpanel4\Shipping\Actions\UpdateShippingZone;
use Bittacora\Bpanel4\Shipping\Dtos\UpdateShippingZoneDto;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\LivewireCountryStateSelector\Tests\Feature\Factories\StateFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

final class UpdateShippingZoneTest extends TestCase
{
    use RefreshDatabase;

    private const NOMBRE_DE_ZONA_EDITADO = 'Nombre de zona editado';
    private StateFactory $stateFactory;
    private UpdateShippingZone $updateSippingZone;

    public function setUp(): void
    {
        parent::setUp();
        $this->updateSippingZone = $this->app->make(UpdateShippingZone::class);
        $this->stateFactory = $this->app->make(StateFactory::class);
    }

    /**
     * @throws ValidationException
     */
    public function testActualizaUnaZonaDeEnvio(): void
    {
        // Arrange
        $zone = $this->getShippingZone();
        $dto = new UpdateShippingZoneDto($zone->getId(), self::NOMBRE_DE_ZONA_EDITADO, false, [$this->stateFactory->getStateId()]);

        // Act
        $this->updateSippingZone->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_zones', [
            'name' => self::NOMBRE_DE_ZONA_EDITADO,
            'active' => false,
            'id' => $zone->getId(),
        ]);
    }

    public function getShippingZone(): ShippingZone
    {
        $shippingZone = new ShippingZone();
        $shippingZone->setName('Zona de envío');
        $shippingZone->setActive(true);
        $shippingZone->save();

        return $shippingZone;
    }
}
