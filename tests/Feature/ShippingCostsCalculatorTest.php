<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Tests\Feature;

use Bittacora\Bpanel4\Clients\Tests\Integration\ObjectMothers\TestClientObjectMother;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Orders\Tests\Integration\Factories\CartFactory;
use Bittacora\Bpanel4\Shipping\Exceptions\InvalidShippingMethodPriceCalculatorException;
use Bittacora\Bpanel4\Shipping\Exceptions\ShippingOptionNotFoundException;
use Bittacora\Bpanel4\Shipping\FixedAmount\Tests\Feature\Factories\FixedAmountFactory;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FixedAmount;
use Bittacora\Bpanel4\Shipping\Services\ShippingCostsCalculator;
use Bittacora\Bpanel4\Shipping\Services\ShippingMethodPriceCalculatorFactory;
use Bittacora\Bpanel4\Shipping\Tests\Feature\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\Types\ShippingOption;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Throwable;

final class ShippingCostsCalculatorTest extends TestCase
{
    use RefreshDatabase;

    private Cart $cart;
    private FixedAmount $fixedAmount;
    private ShippingCostsCalculator $shippingCostsCalculator;

    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function setUp(): void
    {
        parent::setUp();

        /** @var CartFactory $cartFactory */
        $cartFactory = $this->app->make(CartFactory::class);
        $this->shippingCostsCalculator = new ShippingCostsCalculator(new ShippingMethodPriceCalculatorFactory());
        $shippingZoneFactory = new ShippingZoneFactory();
        /** @var TestClientObjectMother $clientObjectMother */
        $clientObjectMother = $this->app->make(TestClientObjectMother::class);
        $fixedAmountFactory = new FixedAmountFactory();

        $this->cart = $cartFactory->createCart();
        $client = $clientObjectMother->getClient();
        $this->cart->setClient($client);
        $this->cart->setShippingAddress($client->getShippingAddress());
        $state = $client->getShippingAddress()->getState();
        $shippingZone = $shippingZoneFactory->getShippingZone($state);
        $this->fixedAmount = $fixedAmountFactory->getFixedAmount($shippingZone);
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws Throwable
     */
    public function testObtieneLasOpcionesDeEnvioDisponibles(): void
    {
        $options = $this->shippingCostsCalculator->getAvailableOptions($this->cart);
        /** @phpstan-ignore-next-line Aunque por tipos siempre se cumple, dejo el assert */
        self::assertIsArray($options);
        /** @phpstan-ignore-next-line Aunque por tipos siempre se cumple, dejo el assert */
        self::assertInstanceOf(ShippingOption::class, $options[0]);
    }

    /**
     * @throws InvalidShippingMethodPriceCalculatorException
     * @throws ShippingOptionNotFoundException
     */
    public function testCalculaLosGastosDeEnvio(): void
    {
        $shippingCosts = $this->shippingCostsCalculator->calculateShippingCosts(
            $this->cart,
            $this->fixedAmount,
        );

        self::assertEquals(10, $shippingCosts?->toFloat());
    }
}
