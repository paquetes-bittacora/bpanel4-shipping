<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Tests\Feature\Factories;

use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Foundation\Testing\WithFaker;

final class ShippingZoneFactory
{
    use WithFaker;

    public function __construct()
    {
        $this->setUpFaker();
    }

    public function getShippingZone(State $state): ShippingZone
    {
        $shippingZone = new ShippingZone();
        $shippingZone->setName($this->faker->name());
        $shippingZone->setActive(true);
        $shippingZone->save();
        $shippingZone->setStates([$state->getId()]);

        return $shippingZone;
    }
}
