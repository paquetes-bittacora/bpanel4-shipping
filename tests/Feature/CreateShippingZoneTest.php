<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Tests\Feature;

use Bittacora\Bpanel4\Shipping\Actions\CreateShippingZone;
use Bittacora\Bpanel4\Shipping\Dtos\CreateShippingZoneDto;
use Bittacora\LivewireCountryStateSelector\Tests\Feature\Factories\StateFactory;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

final class CreateShippingZoneTest extends TestCase
{
    use RefreshDatabase;

    private CreateShippingZone $createShippingZoneDto;
    private StateFactory $stateFactory;

    public function setUp(): void
    {
        parent::setUp();

        $this->createShippingZoneDto = $this->app->make(CreateShippingZone::class);
        $this->withoutExceptionHandling();
        $this->stateFactory = $this->app->make(StateFactory::class);
    }

    /**
     * @throws ValidationException
     * @throws Exception
     */
    public function testCreaUnaZonaDeEnvio(): void
    {
        // Arrange
        $name = 'Nombre de la zona de envío ' . random_int(0, 10000);
        $dto = new CreateShippingZoneDto($name, [$this->stateFactory->getStateId()], true);

        // Act
        $this->createShippingZoneDto->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_zones', ['name' => $name]);
    }

    public function testNoSePuedenCrearDosZonasDeEnvioConElMismoNombre(): void
    {
        // Arrange
        $dto = new CreateShippingZoneDto('Nombre de la zona', [$this->stateFactory->getStateId()], true);
        $this->expectException(ValidationException::class);

        // Act
        $this->createShippingZoneDto->handle($dto);
        $this->createShippingZoneDto->handle($dto);
        // Assert
    }
}
